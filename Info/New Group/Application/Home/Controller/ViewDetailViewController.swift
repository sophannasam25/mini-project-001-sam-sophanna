//
//  ViewDetailViewController.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//

import UIKit
import Kingfisher

class ViewDetailViewController: UIViewController {
    
    @IBOutlet weak var image: UIImageView!
    @IBOutlet weak var auther: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var dates: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    //    @IBOutlet weak var img: UIImageView!\
    
    var article: ArticleModel?
    override func viewDidLoad() {
        super.viewDidLoad()
        
        desc.text = article?.description
        titleLabel.text = article?.title
        dates.text = article?.date
        auther.text = article?.byAuthor
        let url = URL(string: article?.imageUrl ?? "")
        
        let defaultImage = UIImage(named: "defaultimage.jpg")
        
        self.image.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])
    }
}
