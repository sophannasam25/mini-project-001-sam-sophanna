//
//  HomeViewController.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//

import UIKit
import SwiftyJSON
import ProgressHUD
import Alamofire

class HomeViewController: UIViewController {

    @IBOutlet weak var tableView: UITableView!
    
    var articles: [ArticleModel] = []
    var imageData: Data?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.refreshControl = UIRefreshControl()
        // add target to UIRefreshControl
        tableView.refreshControl?.addTarget(self, action: #selector(refresh), for: .valueChanged)
        fetch()
//        fetchs()
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "detailArticle" {
            if let desVC = segue.destination as? ViewDetailViewController, let indexPath = sender as? IndexPath{

                let articles = self.articles[indexPath.row]
                
                desVC.article = articles
                
                
            }
            
        }
    }
    
    @objc func refresh(){
        fetch()
    }

    func fetch(){
        ProgressHUD.show()
        
        HomeViewModel.shared.fetch {[weak self] articles in
            self?.articles = articles
            self?.tableView.reloadData()
        }
        
        Network.shared.fetchArticles { result in
            switch result{
            case .success(let articles):
                
                ProgressHUD.showSucceed("ទាញទិន្នន័យបានជោគជ័យ!")
                self.tableView.refreshControl?.endRefreshing()
                
            case .failure(let error):
                ProgressHUD.showError(error.localizedDescription)
                self.tableView.refreshControl?.endRefreshing()
            }
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        performSegue(withIdentifier: "detailArticle", sender: indexPath)
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let deleteContext = UIContextualAction(style: .destructive, title: "Remove", handler:{
            _, _, _ in
            print("Deleted")
            let article = self.articles[indexPath.row]
            Network.shared.deleteArticle(articleID: article.id)
            self.articles.remove(at: indexPath.row)
            self.tableView.deleteRows(at: [indexPath], with: .fade)
            
        })
        
        let editContext = UIContextualAction(style: .normal, title: "edit", handler:{
            _, _, _ in
            print("edited")
            let article = self.articles[indexPath.row]
           
            let addEditVC = self.storyboard?.instantiateViewController(withIdentifier: "EditVC") as! PostViewController
            
//            self.present(addEditVC,animated: true, completion: nil)
            addEditVC.updateArticle = article
            addEditVC.isUpdated = true
            self.navigationController?.pushViewController(addEditVC, animated: true)
            
        })
        
        editContext.backgroundColor = .systemOrange
        editContext.image = UIImage(systemName: "square.and.pencil")

        
        deleteContext.image = UIImage(systemName: "trash.fill")
        let config = UISwipeActionsConfiguration(actions: [deleteContext,editContext])
        
        config.performsFirstActionWithFullSwipe = false
        return config
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        self.articles.count
    }
    

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! ArticleTableViewCell

        cell.config(articles: self.articles[indexPath.row])
//cell.config(articles: self.articles[indexPath.row].byAuthor)
        cell.selectionStyle = .none
        return cell
    }
    
    
    
    
}
