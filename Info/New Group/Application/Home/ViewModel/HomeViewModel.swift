//
//  HomeView.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//

import Foundation

struct HomeViewModel {

    static let shared = HomeViewModel()
    
    func fetch(completion: @escaping ([ArticleModel])->()){
        Network.shared.fetchArticles { result in
            

                switch result {
                    
                    case .success(let articles):
                    print("Successfull")
                    var modelArticles: [ArticleModel] = articles.compactMap(ArticleModel.init)

                    
                        completion(modelArticles)
                    case .failure(let error):
                    print(error.localizedDescription)
                    
                }
            
            
           
            
        }
    }
    
}
