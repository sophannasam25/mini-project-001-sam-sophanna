//
//  UIView+Extension.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//

import UIKit

extension UIView {
    @IBInspectable var cornerRadius: CGFloat{
        get { return cornerRadius }
        set {
            self.layer.cornerRadius = newValue
        }
    }
}
