//
//  ArticleTableViewCell.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//

import UIKit
import Kingfisher
class ArticleTableViewCell: UITableViewCell {

    @IBOutlet weak var auth: UILabel!
    @IBOutlet weak var desc: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
    override func awakeFromNib() {
        super.awakeFromNib()
      
        // Initialization code
    }

    func config(articles: ArticleModel){
        self.title.text = articles.title
        self.desc.text = articles.description
        self.dateLabel.text = articles.date
        self.auth.text = articles.byAuthor
        let url = URL(string: articles.imageUrl)
        
        let defaultImage = UIImage(named: "defaultimage.jpg")
        
        self.imgView.kf.setImage(with: url,placeholder: defaultImage, options: [.transition(.fade(0.25))])
    }

}
