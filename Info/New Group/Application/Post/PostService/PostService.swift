//
//  PostService.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//
//
//import Foundation
//import Alamofire
//import SwiftyJSON
//
//
//enum postError: Error {
//    case invalidURL
//    case noResponse
//    case unknownError
//}
//
//struct Post {
//    static let shared = Post()
////
//    let baseURL = "http://110.74.194.124:3034/api"
//
//    func postArticle(title: String?, description: String?, imageURL: String?, completion: @escaping(Result<String, Error>)->()){
//        
//        let article: [String:Any] = [
//            "title": title ?? "no title",
//            "description": description ?? "no description",
//            "image": imageURL ?? "no image"
//        ]
//        
//        AF.request("\(baseURL)/articles",method: .post, parameters: article, encoding: JSONEncoding.default).response { response in
//            if let error = response.error {
//                completion(.failure(error))
//            }else{
//                
//                guard let data = response.data else {
//                    return
//                }
//                
//                let jsonData = try! JSON(data: data)
//                
//                completion(.success(jsonData["message"].stringValue))
//            }
//            
//        }
//        
//    }
//    
//    
//    func uploadImage(imageData: Data?,completion: @escaping(String?)->()){
//
//        if let safeData = imageData {
//            AF.upload(multipartFormData: { multiform in
//                multiform.append(safeData, withName: "image", fileName: "test", mimeType: "image/jpeg")
//            }, to: "\(baseURL)/images").response { response in
//
//                if let error = response.error{
//                    print(error.localizedDescription)
//                }
//
//                if let data = response.data {
//
//                    let jsonData = try! JSON(data: data)
//
//                    print(jsonData)
//
//                    let url = jsonData["url"].stringValue
//
//                    completion(url)
//
//                }
//
//
//            }
//        }else{
//            completion(nil)
//        }
//
//
//    }
//   
//
//}
