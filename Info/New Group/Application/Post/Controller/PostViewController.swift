//
//  PostViewController.swift
//  Info
//
//  Created by Phanna on 28/11/21.
//

import UIKit
import ProgressHUD
import Kingfisher

class PostViewController: UIViewController {

    @IBOutlet weak var post: UIButton!
    @IBOutlet weak var descPost: UITextView!
    @IBOutlet weak var titlePost: UITextView!
    @IBOutlet weak var imagePost: UIImageView!
    
    var article: ArticleModel?
    var isUpdated = false
    var updateArticle: ArticleModel?
    
    var pickerView = UIImagePickerController()
    let alertCon = UIAlertController(title: "Choose Options", message: nil, preferredStyle: .actionSheet)
    
    var imageData: Data?
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //2 Set up Gesture
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(showOptions))
        
        self.imagePost.isUserInteractionEnabled = true
        self.imagePost.addGestureRecognizer(tapGesture)
        
        pickerView.delegate = self
        prepareOptions()

        if isUpdated {
           prepareData()
        }
    }
    
    func prepareData(){
        self.post.setTitle("Edit", for: .normal)
        self.titlePost.text = updateArticle?.title
        self.descPost.text = updateArticle?.description
        self.imagePost.kf.setImage(with: URL(string: updateArticle?.imageUrl ?? "None"), placeholder: UIImage(systemName: "Camera.fill"), options: [.transition(.fade(0.5))])
    }
    
    func chooseOptions(option: UIImagePickerController.SourceType){
        self.pickerView.allowsEditing = true
        self.pickerView.mediaTypes = ["public.image"]
        self.pickerView.sourceType = option
        
        present(self.pickerView, animated: true, completion: nil)
      
    }
    
    func prepareOptions(){
        let camera = UIAlertAction(title: "Camera", style: .default) { _ in
            self.chooseOptions(option: .camera)
        }
        let gallary = UIAlertAction(title: "Gallary", style: .default) { _ in
            self.chooseOptions(option: .photoLibrary)
        }
        let cancel = UIAlertAction(title: "Cancel", style: .default, handler: nil)
        alertCon.addAction(camera)
        alertCon.addAction(gallary)
        alertCon.addAction(cancel)
    }
    
    @objc func showOptions(){
        present(alertCon, animated: true, completion: nil)
    }
    @IBAction func postPressed(_ sender: Any) {
        
        let title = titlePost.text
        let description = descPost.text
        
        ProgressHUD.show()
        
        if isUpdated {
            Network.shared.uploadImage(imageData: imageData) { url in
                
                guard let article = self.updateArticle else {
                    return
                }
                
                    Network.shared.updateArticle(articleID: article.id, title: title!, description: description!, imageUrl:  url ?? article.imageUrl) { result in
                        
                        switch result{
                            
                        case .success(let message):
                            ProgressHUD.showSucceed(message)
                        case .failure(let error):
                            ProgressHUD.showError(error.localizedDescription)
                    }
                }
            }
        }else{
            Network.shared.uploadImage(imageData: imageData) { url in
                
                Network.shared.postArticle(title: title, description: description, imageURL: url) { result in
                    switch result {
                    case .success(let msg):
                        ProgressHUD.showSucceed(msg)
                    case .failure(let error):
                        ProgressHUD.showError(error.localizedDescription)
                    }
                }
            }
        }
        
        
       
        
    }


}


extension PostViewController: UINavigationControllerDelegate, UIImagePickerControllerDelegate{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
     
        if let possibleImage = info[.editedImage] as? UIImage {
            self.imagePost.image = possibleImage

            self.imageData = possibleImage.jpegData(compressionQuality: 1.0)
            
        }
        
        dismiss(animated: true)
    }
}
